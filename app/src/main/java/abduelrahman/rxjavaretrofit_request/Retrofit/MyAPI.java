package abduelrahman.rxjavaretrofit_request.Retrofit;

import java.util.List;

import abduelrahman.rxjavaretrofit_request.Model.Post;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MyAPI {

    @GET("posts")
    Observable<List<Post>> getPosts();

}
